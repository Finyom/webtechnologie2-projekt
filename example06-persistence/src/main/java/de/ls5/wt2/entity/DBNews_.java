package de.ls5.wt2.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class DBNews_ extends DBIdentified {
    public static String published;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public static String getPublished() {
        return published;
    }

    public static void setPublished(String published) {
        DBNews_.published = published;
    }

}
